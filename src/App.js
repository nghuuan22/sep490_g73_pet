// import logo from "./logo.svg";
import "./App.scss";
import "bootstrap/dist/css/bootstrap.min.css";
import "@fortawesome/fontawesome-free/css/all.min.css";

import Header from "./components/Header";

import Container from "react-bootstrap/Container";
import { ToastContainer } from "react-toastify";
import AppRouters from "./routers/AppRouters";

import TableUsers from "./components/TableUsers";
import Home from "./components/Home";
import Login from "./components/Author/Login";
import { Routes, Route, Link } from "react-router-dom";

function App() {
  return (
    <>
      <div className="app-container">
        <Header />
        <Container>
          <AppRouters />
          {/* <Routes>
            <Route path="/" element={<Home />} />

            <Route path="/login" element={<Login />} />
            <Route path="/users" element={<TableUsers />} />
          </Routes> */}
        </Container>
      </div>

      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="light"
      />
    </>
  );
}

export default App;
