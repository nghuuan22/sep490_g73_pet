import { useEffect, useState } from "react";
import "./TableUsers.scss";
import Table from "react-bootstrap/Table";
import { fetchAllUser } from "../services/UserService";
import ReactPaginate from "react-paginate";
import ModalAddUser from "./CRUD/ModalAddUser";
import ModalEditUser from "./CRUD/ModalEditUser";
import _ from "lodash";
import ModalDeleteUser from "./CRUD/ModalDeleteUser";

const TableUsers = (props) => {
  const [listUsers, setListUsers] = useState([]);

  const [totalUsers, setTotalUsers] = useState(0);

  const [totalPages, setTotalPages] = useState();

  const [showModalAddUser, setShowModalAddUser] = useState(false);
  const [showModalEditUser, setShowModalEditUser] = useState(false);
  const [showModalDeleteUser, setShowModalDeleteUser] = useState(false);

  //Ham sort
  const [sortBy, setSortBy] = useState("asc");
  const [sortField, setSortFeild] = useState("id");

  //  Hàm data giúp lưu thông tin data Edit de truyen sang ModalEditUser
  const [dataUserEdit, setDataUserEdit] = useState({});
  const [dateUserDelete, setDataUserDelete] = useState({});

  const handleDeleteUser = (user) => {
    setShowModalDeleteUser(true);
    setDataUserDelete(user);
    console.log(user);
  };

  const handleEditUser = (user) => {
    // set bien giup React luu data lai
    setDataUserEdit(user);
    //cho model Edit mo ra
    setShowModalEditUser(true);
  };

  // đóng modal thì không bao giờ hiện lên
  const handleClose = () => {
    setShowModalAddUser(false);
    setShowModalEditUser(false);
    setShowModalDeleteUser(false);
  };

  const handUpdateTable = (user) => {
    setListUsers([user, ...listUsers]);
  };

  // Câp nhật danh sách sau khi Edit User
  const handleEditUserFromModal = (user) => {
    // thu vien lodash
    let cloneListUsers = _.cloneDeep(listUsers);
    let index = listUsers.findIndex((item) => item.id === user.id);

    cloneListUsers[index].first_name = user.first_name;

    setListUsers(cloneListUsers);
  };

  const handleDeleteUserFromModal = (user) => {
    // thu vien lodash
    let cloneListUsers = _.cloneDeep(listUsers);
    cloneListUsers = cloneListUsers.filter((item) => item.id !== user.id);
    setListUsers(cloneListUsers);
  };

  useEffect(() => {
    //call apis
    // dry
    getUsers(1);
  }, []);

  const getUsers = async (page) => {
    let res = await fetchAllUser(page);
    //Check dieu kien
    if (res && res.data) {
      console.log(res);
      setTotalUsers(res.total);
      setListUsers(res.data);
      setTotalPages(res.total_pages);
    }
  };

  // check xem cos data ko
  // console.log(listUsers);

  const handlePageClick = (event) => {
    console.log("event lib: ", event);
    getUsers(+event.selected + 1);
  };

  // Ham truyen tham so Sort
  const handleSort = (sortBy, sortField) => {
    // luu bien
    setSortBy(sortBy);
    setSortFeild(sortField);
    let cloneListUsers = _.cloneDeep(listUsers);
    cloneListUsers = _.orderBy(cloneListUsers, [sortField], [sortBy]);
    setListUsers(cloneListUsers);
  };

  return (
    <>
      <div className="my-3 add-new">
        <h3>
          <b>List User:</b>
        </h3>
        <button
          className="btn btn-primary"
          onClick={() => setShowModalAddUser(true)}
        >
          Add
        </button>
      </div>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>
              <div className="sort-header">
                <span>ID</span>
                <i
                  class="fa-solid fa-arrow-down"
                  onClick={() => handleSort("desc", "id")}
                ></i>
                <i
                  class="fa-solid fa-arrow-up"
                  onClick={() => handleSort("asc", "id")}
                ></i>
              </div>
            </th>
            <th>Email</th>
            <th>
              <div className="sort-header">
                <span>First Name</span>
                <i
                  class="fa-solid fa-arrow-down"
                  onClick={() => handleSort("desc", "first_name")}
                ></i>
                <i
                  class="fa-solid fa-arrow-up"
                  onClick={() => handleSort("asc", "first_name")}
                ></i>
              </div>
            </th>
            <th>Last Name</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {listUsers &&
            listUsers.length > 0 &&
            listUsers.map((item, index) => {
              return (
                <tr key={`users-${index}`}>
                  <td>{item.id}</td>
                  <td>{item.email}</td>
                  <td>{item.first_name}</td>
                  <td>{item.last_name}</td>
                  <td>
                    <button
                      className="btn btn-warning mx-3"
                      onClick={() => handleEditUser(item)}
                    >
                      Edit
                    </button>
                    <button
                      onClick={() => handleDeleteUser(item)}
                      className="btn btn-danger"
                    >
                      Delete
                    </button>
                  </td>
                </tr>
              );
            })}
        </tbody>
      </Table>

      {/* Paging */}
      <ReactPaginate
        breakLabel="..."
        nextLabel="next >"
        onPageChange={handlePageClick}
        pageRangeDisplayed={5}
        pageCount={totalPages}
        previousLabel="< previous"
        pageClassName="page-item"
        pageLinkClassName="page-link"
        nextClassName="page-item"
        nextLinkClassName="page-link"
        previousClassName="page-item"
        previousLinkClassName="page-link"
        breakClassName="page-item"
        breakLinkClassName="page-link"
        containerClassName="pagination"
        activeClassName="active"
      />

      <ModalAddUser
        show={showModalAddUser}
        handleClose={handleClose}
        handUpdateTable={handUpdateTable}
      />

      <ModalEditUser
        show={showModalEditUser}
        dataUserEdit={dataUserEdit}
        handleClose={handleClose}
        handleEditUserFromModal={handleEditUserFromModal}
      />

      <ModalDeleteUser
        show={showModalDeleteUser}
        handleClose={handleClose}
        dateUserDelete={dateUserDelete}
        handleDeleteUserFromModal={handleDeleteUserFromModal}
      />
    </>
  );
};

export default TableUsers;
