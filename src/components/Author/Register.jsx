import { useState } from "react";
import "./Register.scss";
// import { RegisterApi } from "../../services/UserService";
import { toast } from "react-toastify";
import { useNavigate } from "react-router-dom";

const Register = () => {
  // Ham tra ve trang khac
  const navigate = useNavigate();

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  // Ham show password
  const [showPassword, setShowPassword] = useState(false);
  // Ham loading API
  const [showLoadingAPI, setShowLoadingAPI] = useState(false);

  const handleRegister = async () => {
    // if (!email || !password) {
    //   toast.error("Email or Password is required! ");
    //   return;
    // }
    // setShowLoadingAPI(true);
    // let res = await RegisterApi(email, password);
    // console.log("Check API Register", res);
    // if (res && res.token) {
    //   localStorage.setItem("Token:", res.token);
    //   // toast.success("Register success!");
    //   navigate("/users");
    // } else {
    //   //error
    //   if (res && res.status === 400) {
    //     toast.error(res.data.error);
    //     navigate("/users");
    //   }
    // }

    // setShowLoadingAPI(false);
  };

  return (
    <div className="login-backgroud">
      <div className="login-container">
        <div className="login-content row">
          <form action="">
            <div className="col-12 login-text">Register</div>
            <div className="col-12 form-group login-input">
              <label htmlFor="email">Email "eve.holt@reqres.in"</label>
              <input
                type="email"
                placeholder="Enter your email"
                className="form-control"
                value={email}
                onChange={(event) => setEmail(event.target.value)}
              />
            </div>
            <div className="col-12 form-group login-input">
              <label>Password</label>
              <div className="login-password">
                <input
                  type={showPassword === true ? "text" : "password"}
                  placeholder="Enter your Password"
                  className="form-control"
                  value={password}
                  onChange={(event) => setPassword(event.target.value)}
                />
                <i
                  className={
                    showPassword === true
                      ? "fa-solid fa-eye"
                      : "fa-solid fa-eye-slash"
                  }
                  onClick={() => setShowPassword(!showPassword)}
                ></i>
              </div>
            </div>

            <div className="col-12 form-group login-input">
              <label>Confirm Password</label>
              <div className="login-password">
                <input
                  type={showPassword === true ? "text" : "password"}
                  placeholder="Enter your Password"
                  className="form-control"
                  value={password}
                  onChange={(event) => setPassword(event.target.value)}
                />
                <i
                  className={
                    showPassword === true
                      ? "fa-solid fa-eye"
                      : "fa-solid fa-eye-slash"
                  }
                  onClick={() => setShowPassword(!showPassword)}
                ></i>
              </div>
            </div>

            <div className="col-12 login-btn">
              <button
                className={email && password ? "active" : ""}
                disabled={email && password ? false : true}
                onClick={() => handleRegister()}
              >
                {showLoadingAPI && <i className="fas fa-spinner fa-spin"></i>}
                Register
              </button>
            </div>

            <div className="col-12 login-href">
                <div className="col-6">
                <span >Already have a account?</span>
                </div>
              
              <div className="col-6 ">
              <a  href="/login">Login Now</a>
              </div>
              
            </div>

            
          </form>
        </div>
      </div>
    </div>
  );
};

export default Register;
