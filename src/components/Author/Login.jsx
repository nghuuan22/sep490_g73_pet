import { useState } from "react";
import "./Login.scss";
import { loginApi } from "../../services/UserService";
import { toast } from "react-toastify";
import { useNavigate } from "react-router-dom";

const Login = () => {
  // Ham tra ve trang khac
  const navigate = useNavigate();

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  // Ham show password
  const [showPassword, setShowPassword] = useState(false);
  // Ham loading API
  const [showLoadingAPI, setShowLoadingAPI] = useState(false);

  const handleLogin = async () => {
    if (!email || !password) {
      toast.error("Email or Password is required! ");
      return;
    }
    setShowLoadingAPI(true);
    let res = await loginApi(email, password);
    console.log("Check API login", res);
    if (res && res.token) {
      localStorage.setItem("Token:", res.token);
      // toast.success("Login success!");
      navigate("/users");
    } else {
      //error
      if (res && res.status === 400) {
        toast.error(res.data.error);
        navigate("/users");
      }
    }

    setShowLoadingAPI(false);
  };

  return (
    <div className="login-backgroud">
      <div className="login-container">
        <div className="login-content row">
          <form action="">
            <div className="col-12 login-text">Login</div>
            <div className="col-12 form-group login-input">
              <label htmlFor="email">Email "eve.holt@reqres.in"</label>
              <input
                type="email"
                placeholder="Enter your email"
                className="form-control"
                value={email}
                onChange={(event) => setEmail(event.target.value)}
              />
            </div>
            <div className="col-12 form-group login-input">
              <label htmlFor="password">Password</label>
              <div className="login-password">
                <input
                  type={showPassword === true ? "text" : "password"}
                  placeholder="Enter your Password"
                  className="form-control"
                  value={password}
                  onChange={(event) => setPassword(event.target.value)}
                />
                <i
                  className={
                    showPassword === true
                      ? "fa-solid fa-eye"
                      : "fa-solid fa-eye-slash"
                  }
                  onClick={() => setShowPassword(!showPassword)}
                ></i>
              </div>
            </div>

            <div className="col-12 login-btn">
              <button
                className={email && password ? "active" : ""}
                disabled={email && password ? false : true}
                onClick={() => handleLogin()}
              >
                {showLoadingAPI && <i className="fas fa-spinner fa-spin"></i>}
                Login
              </button>
            </div>

            <div className="col-12 ">
              <span className="col-6 forgot-password">Forgot your password?</span>
              <a className="col-6 forgot-password" href="/register">Sign Up</a>
            </div>

            <div className="col-12 text-center">
              <span className="">Or login with:</span>
            </div>

            <div className="col-12 login-social">
              <i className="fab fa-google login-google"></i>
              <i className="fab fa-facebook-f login-facebook"></i>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default Login;
