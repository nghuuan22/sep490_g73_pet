import { useState } from "react";
import { Modal, Button } from "react-bootstrap";
import { deleteUser } from "../../services/UserService";
import { toast } from "react-toastify";

const ModalDeleteUser = (props) => {
  const { show, handleClose, dateUserDelete, handleDeleteUserFromModal } =
    props;

  const confirmDelete = async () => {
    let res = await deleteUser(dateUserDelete.id);
    // Thêm dấu + để tránh trường hợp trả ra chuỗi String thì sẽ chuyển về kiểu số nguyên
    if (res && +res.statusCode === 204) {
      toast.success("Delete user successfully!");
      handleClose();
      handleDeleteUserFromModal(dateUserDelete);
    } else {
      toast.error("Delete user error!");
    }
    console.log("Check Delete ID:", res);
  };

  return (
    <>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Delete a User</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="body-add">
            <span>
              Do you want to delete this user?
              <br /> Email = {dateUserDelete.email}
            </span>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Cancel
          </Button>
          <Button variant="primary" onClick={() => confirmDelete()}>
            Confirm
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default ModalDeleteUser;
