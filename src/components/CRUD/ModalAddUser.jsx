import { useState } from "react";
import { Modal, Button } from "react-bootstrap";
import { postCreateUser } from "../../services/UserService";
import { toast } from "react-toastify";

const ModalAddUser = (props) => {
  const { show, handleClose, handUpdateTable } = props;

  const [name, setName] = useState("");

  const [job, setJob] = useState("");

  const handSaveUser = async () => {
    let save = await postCreateUser(name, job);

    if (save && save.id) {
      handleClose();
      setName("");
      setJob("");
      toast.success("A user is created success!");
      handUpdateTable({ first_name: name, id: save.id });
      // success
    } else {
      toast.error("Error!");
      // error
    }
  };

  return (
    <>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Add new user</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="body-add">
            <div className="mb-3">
              <label className="form-label">Name</label>
              <input
                type="text"
                className="form-control"
                value={name}
                onChange={(event) => setName(event.target.value)}
              />
            </div>
            <div className="mb-3">
              <label className="form-label">Job</label>
              <input
                type="text"
                className="form-control"
                value={job}
                onChange={(event) => setJob(event.target.value)}
              />
            </div>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={() => handSaveUser()}>
            Add
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default ModalAddUser;
