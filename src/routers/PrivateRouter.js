import { Routes, Route, Link } from "react-router-dom";

const PrivateRouter = (props) => {
  return <>{props.children}</>;
};

export default PrivateRouter;
