import { Routes, Route, Link } from "react-router-dom";
import TableUsers from "../components/TableUsers";
import Home from "../components/Home";
import Login from "../components/Author/Login";
import PrivateRouter from "./PrivateRouter";
import Register from "../components/Author/Register";
import Login2 from "../components/Author/Login2";

const AppRouters = () => {
  return (
    <>
      {/* route giúp khi load lại trang ko tốn time */}
      <Routes>
        <Route path="/" element={<Home />} />

        <Route path="/login" element={<Login />} />
        <Route
          path="/users"
          element={
            <PrivateRouter path="/users">
              <TableUsers />
            </PrivateRouter>
          }
        />
        <Route path="/login2" element={<Login2 />} />
      </Routes>
    </>
  );
};

export default AppRouters;
